```
which caddy
/root/golang/packages/bin/caddy
```

```
./caddy.sh 

Command Usage: 
./caddy.sh {install|update|uninstall|addsite|start|stop|restart|status|domains|certs|version|help}
```

```
./caddy.sh status
caddy is running (13188)
-----------------------------------------------
Max processes     8192     8192     processes 
Max open files    262144   262144   files 
-----------------------------------------------
```

```
./caddy.sh restart
caddy restarted and is running (13360)
-----------------------------------------------
Max processes     8192     8192     processes 
Max open files    262144   262144   files 
-----------------------------------------------
```

```
ps aufxw | grep caddy | grep -v grep
root     13188  0.0  0.6  38868 12880 pts/1    Sl   14:37   0:00 /usr/local/bin/caddy -log /usr/local/nginx/logs/caddy.log -quiet=true -agree=true -conf=/etc/caddy/Caddyfile -root=/var/tmp -pidfile=/var/run/caddy/caddy.pid
```

```
caddy -version
Caddy 0.10.9 (+545fa84 Fri Sep 15 14:36:05 UTC 2017)
1 file changed, 32 insertions(+)
caddy/caddymain/run.go
```

```
curl -I http://$(hostname):8888           
HTTP/1.1 200 OK
Accept-Ranges: bytes
Cache-Control: max-age=86400
Content-Length: 3801
Content-Type: text/html; charset=utf-8
Etag: "ow5yjt2xl"
Last-Modified: Tue, 12 Sep 2017 10:59:53 GMT
Server: Caddy
X-Powered-By: Caddy via CentminMod
Date: Fri, 15 Sep 2017 14:44:28 GMT
```

```
caddy -plugins
Server types:
  http

Caddyfile loaders:
  short
  flag
  default

Other plugins:
  http.authz
  http.awses
  http.awslambda
  http.basicauth
  http.bind
  http.browse
  http.cache
  http.cgi
  http.cors
  http.datadog
  http.errors
  http.expires
  http.expvar
  http.ext
  http.fastcgi
  http.filter
  http.git
  http.gopkg
  http.grpc
  http.gzip
  http.header
  http.hugo
  http.index
  http.internal
  http.ipfilter
  http.jekyll
  http.jsonp
  http.jwt
  http.limits
  http.locale
  http.log
  http.login
  http.mailout
  http.markdown
  http.mime
  http.minify
  http.multipass
  http.nobots
  http.pprof
  http.prometheus
  http.proxy
  http.proxyprotocol
  http.push
  http.ratelimit
  http.realip
  http.reauth
  http.redir
  http.request_id
  http.restic
  http.rewrite
  http.root
  http.search
  http.status
  http.templates
  http.timeouts
  http.upload
  http.webdav
  http.websocket
  shutdown
  startup
  tls
  tls.storage.file
```

```
cat /etc/caddy/conf.d/hostname.conf 
hostname:8888 {
    gzip {
        level 5
        min_length 1400
    }
    #browse
    header / {
        Cache-Control "max-age=86400"
        #X-Content-Type-Options "nosniff"
        #X-Frame-Options "SAMEORIGIN"
        #X-XSS-Protection "1; mode=block"
        X-Powered-By "Caddy via CentminMod"
        #-Server
    }   
    tls off
    root /usr/local/nginx/html
    fastcgi / 127.0.0.1:9000 php { }
    errors /usr/local/nginx/logs/caddy-hostname-errors.log {
        rotate_size 100 # Rotate after 100 MB
        rotate_age  14  # Keep log files for 14 days
        rotate_keep 10  # Keep at most 10 log files
        rotate_compress
        #404 404.html # Not Found
        #500 500.html # Internal Server Error
      }
    log / /usr/local/nginx/logs/caddy-hostname-access.log "{remote} {when} {method} {uri} {proto} {status} {size} {>User-Agent} {latency}" {
        rotate_size 100 # Rotate after 100 MB
        rotate_age  14  # Keep log files for 14 days
        rotate_keep 10  # Keep at most 10 log files
        rotate_compress
    }
}
```