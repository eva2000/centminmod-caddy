#!/bin/bash
######################################################
# caddy server installer for centminmod.com LEMP stack
# custom built caddy binary without HTTP sponsor header
# using forked caddyman builder https://github.com/centminmod/caddyman/tree/cmm
# runs caddy on port 8888 by default
# written by George Liu (eva2000) centminmod.com
# curl https://getcaddy.com | bash -s dns,hook.service,http.awslambda,http.cgi,http.cors,http.datadog,http.expires,http.filemanager,http.filter,http.git,http.hugo,http.ipfilter,http.jwt,http.mailout,http.minify,http.prometheus,http.proxyprotocol,http.ratelimit,http.realip,http.upload,net,tls.dns.cloudflare,tls.dns.digitalocean,tls.dns.dnsimple,tls.dns.dnspod,tls.dns.dyn,tls.dns.exoscale,tls.dns.gandi,tls.dns.googlecloud,tls.dns.linode,tls.dns.namecheap,tls.dns.ovh,tls.dns.rackspace,tls.dns.rfc2136,tls.dns.route53,tls.dns.vultr
# https://caddyserver.com/download/linux/amd64?plugins=dns,hook.service,http.awslambda,http.cgi,http.cors,http.datadog,http.expires,http.filemanager,http.filter,http.git,http.hugo,http.ipfilter,http.jwt,http.mailout,http.minify,http.prometheus,http.proxyprotocol,http.ratelimit,http.realip,http.upload,net,tls.dns.cloudflare,tls.dns.digitalocean,tls.dns.dnsimple,tls.dns.dnspod,tls.dns.dyn,tls.dns.exoscale,tls.dns.gandi,tls.dns.googlecloud,tls.dns.linode,tls.dns.namecheap,tls.dns.ovh,tls.dns.rackspace,tls.dns.rfc2136,tls.dns.route53,tls.dns.vultr
######################################################
# variables
#############
VER=0.5
DT=$(date +"%d%m%y-%H%M%S")
DEBUG='n'
SELFSIGN_CERT='y'

CADDYMAN_BIN='y'
CADDY_USER='nginx'
CADDY_BIN='/usr/local/bin/caddy'
#CADDY_BIN='/usr/local/bin/caddy-custom'
CADDY_CONFIGFILE='/etc/caddy/Caddyfile'
CADDY_PIDDIR='/var/run/caddy'
CADDY_PIDFILE='caddy.pid'
CADDY_PID="${CADDY_PIDDIR}/${CADDY_PIDFILE}"
CADDY_LOGFILE='/usr/local/nginx/logs/caddy.log'
CADDY_HTTPPORT='8888'
CADDY_HTTPSPORT='448'
SETCAP='n'

# https://github.com/mholt/caddy/wiki/QUIC
ENABLE_QUIC='n'
CIPHERLIST='ECDHE-RSA-AES128-GCM-SHA256 ECDHE-ECDSA-AES128-GCM-SHA256 ECDHE-RSA-AES128-CBC-SHA ECDHE-RSA-AES256-CBC-SHA ECDHE-ECDSA-AES256-CBC-SHA ECDHE-ECDSA-AES128-CBC-SHA RSA-AES128-CBC-SHA RSA-AES256-CBC-SHA ECDHE-RSA-3DES-EDE-CBC-SHA RSA-3DES-EDE-CBC-SHA'
caddy_hostname=$(uname -n)
CADDY_CERTHOME='/etc/centminmod/caddy'
###############################################################
# pushover API
PUSHALERT='n'
papiurl=https://api.pushover.net/1/messages.json
# registered pushover.net users will find their Pushover email
# aliases for notifications at https://pushover.net/
pushover_email=''
###############################################################
# Setup Colours
black='\E[30;40m'
red='\E[31;40m'
green='\E[32;40m'
yellow='\E[33;40m'
blue='\E[34;40m'
magenta='\E[35;40m'
cyan='\E[36;40m'
white='\E[37;40m'

boldblack='\E[1;30;40m'
boldred='\E[1;31;40m'
boldgreen='\E[1;32;40m'
boldyellow='\E[1;33;40m'
boldblue='\E[1;34;40m'
boldmagenta='\E[1;35;40m'
boldcyan='\E[1;36;40m'
boldwhite='\E[1;37;40m'

Reset="tput sgr0"      #  Reset text attributes to normal
                       #+ without clearing screen.

cecho ()                     # Coloured-echo.
                             # Argument $1 = message
                             # Argument $2 = color
{
message=$1
color=$2
echo -e "$color$message" ; $Reset
return
}

######################################################
if [ ! -d "$CADDY_CERTHOME" ]; then
  mkdir -p "$CADDY_CERTHOME"
fi

# /etc/centminmod/caddy/caddy-config.ini
if [ -f "${CADDY_CERTHOME}/caddy-config.ini" ]; then
  . "${CADDY_CERTHOME}/caddy-config.ini"
fi
######################################################
# functions
#############
CENTOSVER=$(awk '{ print $3 }' /etc/redhat-release)
SCRIPT_DIR=$(readlink -f $(dirname ${BASH_SOURCE[0]}))

if [ "$CENTOSVER" == 'release' ]; then
    CENTOSVER=$(awk '{ print $4 }' /etc/redhat-release | cut -d . -f1,2)
    if [[ "$(awk '{ print $4 }' /etc/redhat-release | cut -d . -f1)" = '7' ]]; then
        CENTOS_SEVEN='7'
    fi
fi

if [[ "$(awk '{ print $3 }' /etc/redhat-release | cut -d . -f1)" = '6' ]]; then
    CENTOS_SIX='6'
fi

if [ "$CENTOSVER" == 'Enterprise' ]; then
    CENTOSVER=$(awk '{ print $7 }' /etc/redhat-release)
    OLS='y'
fi

if [ ! -d /etc/centminmod ]; then
  echo
  echo "Centmin Mod LEMP Stack not detected"
  echo "$0 is written for centminmod.com LEMP"
  echo "integrated usage only ...."
  echo
  exit
fi

if [ ! -d "$CADDY_PIDDIR" ]; then
  mkdir -p "$CADDY_PIDDIR"
  chown "$CADDY_USER" "$CADDY_PIDDIR"
fi

# whitelist
if [[ -f /etc/csf/csf.conf && -z "$(grep "$CADDY_HTTPPORT," /etc/csf/csf.conf)" ]]; then
  CHECK=1
  sed -i "s/TCP_IN = \"/TCP_IN = \"$CADDY_HTTPPORT,/g" /etc/csf/csf.conf
  sed -i "s/TCP6_IN = \"/TCP6_IN = \"$CADDY_HTTPPORT,/g" /etc/csf/csf.conf
  # sed -i "s/UDP_IN = \"/UDP_IN = \"$CADDY_HTTPPORT,/g" /etc/csf/csf.conf
  # sed -i "s/UDP6_IN = \"/UDP6_IN = \"$CADDY_HTTPPORT,/g" /etc/csf/csf.conf
  csf -r >/dev/null 2>&1
fi
if [[ -f /etc/csf/csf.conf && -z "$(grep "$CADDY_HTTPSPORT," /etc/csf/csf.conf)" ]]; then
  CHECK=1
  sed -i "s/TCP_IN = \"/TCP_IN = \"$CADDY_HTTPSPORT,/g" /etc/csf/csf.conf
  sed -i "s/TCP6_IN = \"/TCP6_IN = \"$CADDY_HTTPSPORT,/g" /etc/csf/csf.conf
  csf -r >/dev/null 2>&1
fi
if [[ "$ENABLE_QUIC" = [yY] ]]; then
  if [[ -f /etc/csf/csf.conf && -z "$(egrep 'UDP_IN = |UDP6_IN = ' /etc/csf/csf.conf | grep "$CADDY_HTTPSPORT,")" ]]; then
    CHECK=1
    sed -i "s/UDP_IN = \"/UDP_IN = \"$CADDY_HTTPSPORT,/g" /etc/csf/csf.conf
    sed -i "s/UDP6_IN = \"/UDP6_IN = \"$CADDY_HTTPSPORT,/g" /etc/csf/csf.conf
    csf -r >/dev/null 2>&1
  fi
fi
if [[ "$DEBUG" = [yY] ]]; then
  if [[ -f /etc/csf/csf.conf && "$CHECK" = '1' ]]; then
    egrep --color "$CADDY_HTTPPORT,|$CADDY_HTTPSPORT," /etc/csf/csf.conf
  fi
fi

######################################################
c_ver() {
  echo "$0 ver: $VER"
}

######################################################
pushover_alert() {
  if [[ "$PUSHALERT" = [yY] ]]; then
    if [ ! -z "$pushover_email" ]; then
      push_vhostname="$1"
      push_port="$2"
      acme_domainconf="${CADDY_CERTHOME}/${push_vhostname}/${push_vhostname}.conf"
      echo | openssl s_client -connect "${push_vhostname}":"${push_port}" 2>/dev/null | openssl x509 -noout -dates > "$CADDY_CERTHOME/${vhostname}/${vhostname}.conf"
      acmecreate_date=$(grep "^notBefore=" "$CADDY_CERTHOME/${vhostname}/${vhostname}.conf" | cut -d '=' -f 2)
      acmenextrenew_date=$(grep "^notAfter=" "$CADDY_CERTHOME/${vhostname}/${vhostname}.conf" | cut -d '=' -f 2)
      echo "
      $push_vhostname SSL Cert Created: $acmecreate_date
      $push_vhostname SSL Cert Next Renewal Date: $acmenextrenew_date
      "| mail -s "$push_vhostname SSL Cert Setup `date`" -r "$pushover_email" "$pushover_email"
    fi
  fi
}

######################################################
c_start() {
  if [[ "$CADDY_BIN" = '/usr/local/bin/caddy-custom' ]]; then
    start_getpid=$(ps --no-headers -C caddy-custom | awk '{print $1}')
  else
    start_getpid=$(ps --no-headers -C caddy | awk '{print $1}')
  fi
  if [[ "$ENABLE_QUIC" = [yY] ]]; then
    QUICKOPT=' -quic'
  else
    QUICKOPT=""
  fi
  if [ -z "$start_getpid" ] ; then
    if [[ "$SETCAP" = [yY] ]]; then
      SETCAPRUN='setcap cap_net_bind_service=+ep '
    else
      SETCAPRUN=""
    fi
    if [ ! -f "$CADDY_PID" ]; then
      touch "$CADDY_PID"
    fi
    if [[ "$DEBUG" = [yY] ]]; then
      ${SETCAPRUN}${CADDY_BIN}${QUICKOPT} -log $CADDY_LOGFILE -agree=true -conf=${CADDY_CONFIGFILE} -root=/var/tmp -pidfile=${CADDY_PID} &
    else
      ${SETCAPRUN}${CADDY_BIN}${QUICKOPT} -log $CADDY_LOGFILE -quiet=true -agree=true -conf=${CADDY_CONFIGFILE} -root=/var/tmp -pidfile=${CADDY_PID} &
    fi
    if [[ "$SETCAP" = [yY] ]]; then
      getcap ${CADDY_BIN}
    fi
  else
    echo "caddy already running ($(cat "$CADDY_PID"))"
  fi
}

######################################################
c_stop() {
  if [[ "$CADDY_BIN" = '/usr/local/bin/caddy-custom' ]]; then
    stop_getpid=$(ps --no-headers -C caddy-custom | awk '{print $1}')
  else
    stop_getpid=$(ps --no-headers -C caddy | awk '{print $1}')
  fi
  if [[ "$DEBUG" = [yY] ]]; then
    if [[ "$CADDY_BIN" = '/usr/local/bin/caddy-custom' ]]; then
      ps --no-headers -C caddy-custom | awk '{print $1}'
    else
      ps --no-headers -C caddy | awk '{print $1}'
    fi
  fi
  kill -9 $stop_getpid >/dev/null 2>&1
  if [[ "$CADDY_BIN" = '/usr/local/bin/caddy-custom' ]]; then
    stop_getpid=$(ps --no-headers -C caddy-custom | awk '{print $1}')
  else
    stop_getpid=$(ps --no-headers -C caddy | awk '{print $1}')
  fi
  if [[ -f "$CADDY_PID" || -z "$stop_getpid" ]]; then
    rm -rf "$CADDY_PID"
  fi
}

######################################################
c_status() {
  is_restart=$1
  if [ "$is_restart" = 'restart' ]; then
    restart_msg=' restarted and'
  else
    restart_msg=""
  fi
  if [[ "$CADDY_BIN" = '/usr/local/bin/caddy-custom' ]]; then
    status_getpid=$(ps --no-headers -C caddy-custom | awk '{print $1}')
  else
    status_getpid=$(ps --no-headers -C caddy | awk '{print $1}')
  fi
  if [[ ! -f "$CADDY_PID" && -z "$status_getpid" ]]; then
    echo "caddy is not running"
  elif [[ -f "$CADDY_PID" && -z "$status_getpid" ]]; then
    echo "caddy is not running"
  else
    sleep 3
    echo "caddy${restart_msg} is running ($(cat "$CADDY_PID"))"
    echo "-----------------------------------------------"
    if [[ "$CADDY_BIN" = '/usr/local/bin/caddy-custom' ]]; then
      cat /proc/$(pidof caddy-custom)/limits | egrep "(processes|files)" | sed 's|     | |g'
    else
      cat /proc/$(pidof caddy)/limits | egrep "(processes|files)" | sed 's|     | |g'
    fi
    echo "-----------------------------------------------"
  fi
}

######################################################
c_restart() {
  c_stop
  sleep 3
  c_start
  c_status restart
}

######################################################
c_uninstall() {
  echo -n
}

######################################################
helpmsg() {
  echo
  echo "Command Usage: "
  echo "$0 {install|update|uninstall|addsite|start|stop|restart|status|domains|certs|version|help}"
  echo
}

######################################################
setupcaddy() {
mkdir -p /etc/caddy/conf.d
mkdir -p /etc/caddy
chown -R root:nginx /etc/caddy
mkdir -p /etc/ssl/caddy
chown -R nginx:root /etc/ssl/caddy
chmod 0770 /etc/ssl/caddy
}

######################################################
getcaddy() {
mkdir -p /root/tools
cd /root/tools
if [[ "$CADDYMAN_BIN" = [yY] ]]; then
  git clone -b cmm https://github.com/centminmod/caddyman
  cd caddyman
  ./caddyman.sh install authz awses awslambda cache cgi cors datadog expires filter git gopkg grpc hugo ipfilter jekyll jsonp jwt locale login mailout minify multipass nobots prometheus proxyprotocol ratelimit realip reauth restic search upload webdav
else
  if [[ "$(uname -m)" = 'x86_64' ]]; then
    curl https://getcaddy.com | bash -s personal http.authz,http.awslambda,http.cache,http.cgi,http.cors,http.expires,http.filemanager,http.filter,http.git,http.gopkg,http.hugo,http.ipfilter,http.jwt,http.login,http.mailout,http.minify,http.nobots,http.prometheus,http.proxyprotocol,http.ratelimit,http.realip,http.reauth,http.restic,http.upload,http.webdav,net,tls.dns.cloudflare,tls.dns.digitalocean,tls.dns.googlecloud,tls.dns.linode,tls.dns.namecheap,tls.dns.ovh,tls.dns.route53,tls.dns.vultr
  else
    curl https://getcaddy.com | bash -s personal http.authz,http.awslambda,http.cache,http.cgi,http.cors,http.expires,http.filemanager,http.filter,http.git,http.gopkg,http.hugo,http.ipfilter,http.jwt,http.login,http.mailout,http.minify,http.nobots,http.prometheus,http.proxyprotocol,http.ratelimit,http.realip,http.reauth,http.restic,http.upload,http.webdav,net,tls.dns.cloudflare,tls.dns.digitalocean,tls.dns.googlecloud,tls.dns.linode,tls.dns.namecheap,tls.dns.ovh,tls.dns.route53,tls.dns.vultr
  fi
  if [ -f /root/golang/packages/bin/caddy_old ]; then
    \cp -af /root/golang/packages/bin/caddy_old /usr/local/bin/caddy-custom
  fi
fi
$CADDY_BIN -plugins
}

######################################################
mainvhost_setup() {
cat >/etc/caddy/Caddyfile<<EFF
import ./conf.d/*
EFF


cat >"/etc/caddy/conf.d/${caddy_hostname}.conf"<<EOF
$caddy_hostname:$CADDY_HTTPPORT {
    gzip {
        level 5
        min_length 1400
    }
    #browse
    header / {
        Cache-Control "max-age=86400"
        #X-Content-Type-Options "nosniff"
        #X-Frame-Options "SAMEORIGIN"
        #X-XSS-Protection "1; mode=block"
        X-Powered-By "Caddy via CentminMod"
        #-Server
    }   
    tls off
    root /usr/local/nginx/html
    fastcgi / 127.0.0.1:9000 php { }
    errors /usr/local/nginx/logs/caddy-${caddy_hostname}-errors.log {
        rotate_size 100 # Rotate after 100 MB
        rotate_age  14  # Keep log files for 14 days
        rotate_keep 10  # Keep at most 10 log files
        rotate_compress
        #404 404.html # Not Found
        #500 500.html # Internal Server Error
      }
    log / /usr/local/nginx/logs/caddy-${caddy_hostname}-access.log "{remote} {when} {method} {uri} {proto} {status} {size} {>User-Agent} {latency}" {
        rotate_size 100 # Rotate after 100 MB
        rotate_age  14  # Keep log files for 14 days
        rotate_keep 10  # Keep at most 10 log files
        rotate_compress
    }
}
EOF
}

######################################################
mkvhostdirs() {
if [ ! -d /home/nginx/domains/$vhostname ]; then

echo "creating $vhostname directories"

# Checking Permissions, making directories, example index.html
umask 027
mkdir -p /home/nginx/domains/$vhostname/{public,private,log,backup}

cat > "/home/nginx/domains/$vhostname/public/index.html" <<END
<!DOCTYPE html>
<html lang="en"><head>
  <meta charset="utf-8"><meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="${vhostname} nginx site generated by centminmod.com" name="description">
  <title>${vhostname}</title>
  <link href="//centminmod.com/purecss/pure-min.css" rel="stylesheet"><!--[if lte IE 8]>
  <link rel="stylesheet" href="//centminmod.com/purecss/grids-responsive-old-ie-min.css">
  <![endif]-->
  <!--[if gt IE 8]><!-->
  <link href="//centminmod.com/purecss/grids-responsive-min.css" rel="stylesheet"><!--<![endif]-->
  <!--[if gt IE 8]><!-->
  <style type="text/css">
  *{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}
  a{text-decoration:none;color:#3d92c9}
  a:hover,a:focus{text-decoration:underline}
  h3{font-weight:100}
  .pure-img-responsive{max-width:100%;height:auto}
  #layout{padding:0}
  .header{text-align:center;top:auto;margin:3em auto}
  .sidebar{background:#2e739a;color:#fff}
  .brand-title,.brand-tagline{margin:0}
  .brand-title{text-transform:uppercase}
  .brand-tagline{font-weight:300;color:#b0cadb}
  .nav-list{margin:0;padding:0;list-style:none}
  .nav-item{display:inline-block;*display:inline;zoom:1}
  .nav-item a{background:transparent;border:2px solid #b0cadb;color:#fff;margin-top:1em;letter-spacing:.05em;text-transform:uppercase;font-size:85%}
  .nav-item a:hover,.nav-item a:focus{border:2px solid #3d92c9;text-decoration:none}
  .content-subhead{text-transform:uppercase;color:#aaa;border-bottom:1px solid #eee;padding:.4em 0;font-size:80%;font-weight:500;letter-spacing:.1em}
  .content{padding:2em 1em 0}
  .post{padding-bottom:2em}
  .post-title{font-size:2em;color:#222;margin-bottom:.2em}
  .post-avatar{border-radius:50px;float:right;margin-left:1em}
  .post-description{font-family:Georgia,"Cambria",serif;color:#444;line-height:1.8em}
  .post-meta{color:#999;font-size:90%;margin:0}
  .post-category{margin:0 .1em;padding:.3em 1em;color:#fff;background:#999;font-size:80%}
  .post-category-design{background:#5aba59}
  .post-category-pure{background:#4d85d1}
  .post-category-yui{background:#8156a7}
  .post-category-js{background:#df2d4f}
  .post-images{margin:1em 0}
  .post-image-meta{margin-top:-3.5em;margin-left:1em;color:#fff;text-shadow:0 1px 1px #333}
  .footer{text-align:center;padding:1em 0}
  .footer a{color:#ccc;font-size:80%}
  .footer .pure-menu a:hover,.footer .pure-menu a:focus{background:none}
  @media (min-width: 48em) {
  .content{padding:2em 3em 0;margin-left:25%}
  .header{margin:80% 2em 0;text-align:right}
  .sidebar{position:fixed;top:0;bottom:0}
  }
  </style><!--<![endif]-->
</head>
<body>
  <div class="pure-g" id="layout">
    <div class="sidebar pure-u-1 pure-u-md-1-4">
      <div class="header">
        <h1 class="brand-title">Welcome to ${vhostname}</h1>
        <h2 class="brand-tagline">Powered by CentminMod</h2>
        <h2 class="brand-tagline">Nginx Server</h2>
        <nav class="nav">
          <ul class="nav-list">
            <li class="nav-item">
              <a class="pure-button" href="https://centminmod.com">CentminMod.com</a>
            </li>
            <li class="nav-item">
              <a class="pure-button" href="https://community.centminmod.com">CentminMod Forums</a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
    <div class="content pure-u-1 pure-u-md-3-4">
      <div>
        <!-- A wrapper for all the blog posts -->
        <div class="posts">
          <h1 class="content-subhead">index.html place holder</h1><!-- A single blog post -->
          <section class="post">
            <header class="post-header">
              <h2 class="post-title">${vhostname}</h2>
            </header>
            <div class="post-description">
              <p>Welcome to ${vhostname} generated by integrated Caddy server. This index.html page can be removed.</p>
              <p>Useful Centmin Mod info and links to bookmark.</p>
              <ul>
                <li>Getting Started Guide - <a href="https://centminmod.com/getstarted.html" target="_blank">https://centminmod.com/getstarted.html</a>
                </li>
                <li>Latest Centmin Mod version - <a href="https://centminmod.com" target="_blank">https://centminmod.com</a>
                </li>
                <li>Centmin Mod FAQ - <a href="https://centminmod.com/faq.html" target="_blank">https://centminmod.com/faq.html</a>
                </li>
                <li>Change Log - <a href="https://centminmod.com/changelog.html" target="_blank">https://centminmod.com/changelog.html</a>
                </li>
                <li>Google+ Page latest news <a href="https://plus.google.com/u/0/b/104831941868856035845/104831941868856035845" target="_blank">Centmin Mod Google+</a>
                </li>
                <li>Centmin Mod Community Forum <a href="https://community.centminmod.com/" target="_blank">https://community.centminmod.com/</a>
                </li>
                <li>Centmin Mod Twitter <a href="https://twitter.com/centminmod" target="_blank">https://twitter.com/centminmod</a>
                </li>
                <li>Centmin Mod Facebook Page <a href="https://www.facebook.com/centminmodcom" target="_blank">https://www.facebook.com/centminmodcom</a>
                </li>
                <li>Centmin Mod Medium <a href="https://medium.com/@centminmod" target="_blank">https://medium.com/@centminmod</a>
                </li>
              </ul>
              <p>For Centmin Mod LEMP stack hosting check out <a href="https://www.digitalocean.com/?refcode=c1cb367108e8" target="_blank">Digitalocean</a></p>
            </div>
          </section>
        </div>
        <div class="footer">
          <div class="pure-menu pure-menu-horizontal">
            <ul>
              <li class="pure-menu-item">
                <a class="pure-menu-link" href="#">PureCSS Template BSD Licensed Copyright 2016 Yahoo! Inc. All rights reserved</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
END

umask 022
chown -R nginx:nginx "/home/nginx/domains/$vhostname"
find "/home/nginx/domains/$vhostname" -type d -exec chmod g+s {} \;
fi
}

######################################################
mkvhost() {
  echo
  echo "Add a new Caddy site domain vhost"
  read -ep "Enter domain name wihtout www.: " vhostname
  echo
  echo "Caddy integration into Centmin Mod LEMP runs Nginx on port 80 & 443"
  echo "Caddy server runs on $CADDY_HTTPPORT & $CADDY_HTTPSPORT"
  read -ep "Do you want to create the Nginx vhost for $vhostname as well ? [y/n]: " create_ngxvhost
  read -ep "Create self-signed SSL cert for Nginx vhost $vhostname ? recommended [y/n]: " vhostssl

  if [[ "$create_ngxvhost" = [yY] ]]; then
    if [ -f "/usr/local/src/centminmod/addons/acmetool.sh" ] && [[ "$vhostssl" = 'y' ]]; then
    echo
    cecho "-------------------------------------------------------------" $boldyellow
    echo "ok: /usr/local/src/centminmod/addons/acmetool.sh"
    echo ""/usr/local/src/centminmod/addons/acmetool.sh" issue "$vhostname""
    "/usr/local/src/centminmod/addons/acmetool.sh" issue "$vhostname"
    cecho "-------------------------------------------------------------" $boldyellow
    echo
    elif [ ! -f "/usr/local/src/centminmod/addons/acmetool.sh" ] && [[ "$vhostssl" = 'y' ]]; then
      if [ -f /usr/bin/nv ]; then
        echo
        cecho "-------------------------------------------------------------" $boldyellow
        echo "Generating Nginx vhost for $vhostname"
        cecho "-------------------------------------------------------------" $boldyellow
        ftpusername=$(pwgen -s 15 1)
        echo
        cecho "-------------------------------------------------------------" $boldyellow
        echo "/usr/bin/nv -d "${vhostname}" -s y -u "${ftpusername}""
        cecho "-------------------------------------------------------------" $boldyellow
        echo
        /usr/bin/nv -d "${vhostname}" -s y -u "${ftpusername}"
        echo
      else
        echo
        cecho "-------------------------------------------------------------" $boldyellow
        echo "Error: /usr/bin/nv does not exist"
        cecho "-------------------------------------------------------------" $boldyellow
        echo
        exit
      fi
    fi
  fi

  if [ ! -d "/home/nginx/domains/${vhostname}/public" ]; then
    echo "/home/nginx/domains/${vhostname}/public directory does not exist"
    echo
    echo "--------------------------------------------------------------------"
    echo "caddy.sh was written for centminmod.com LEMP stack environments and"
    echo "requires a Centmin Mod Nginx generated vhost directory to work as"
    echo "Caddy HTTP vhost domains are setup on port $CADDY_HTTPPORT and Caddy HTTPS"
    echo "vhost domains are setup on port $CADDY_HTTPSPORT leaving Centmin Mod Nginx to run"
    echo "on HTTP port 80 and HTTPS port 443"
    echo "--------------------------------------------------------------------"
    echo
    echo "--------------------------------------------------------------------"
    echo "you can override this and setup a Caddy only vhost site on the server"
    echo "however, doing so will prevent the same site being setup on Centmin Mod"
    echo "Nginx server in future"
    echo "--------------------------------------------------------------------"
    read -ep "do you want to override this and setup Caddy only vhost site ? [y/n] " caddyonly
    echo
    if [[ "$caddyonly" == [yY] ]]; then
      mkvhostdirs
    else
    exit
    fi
  fi

  if [ ! -f "/etc/caddy/conf.d/${vhostname}.conf" ] && [ -d "/home/nginx/domains/${vhostname}/public" ]; then
cat >"/etc/caddy/conf.d/${vhostname}.conf"<<EOF
$vhostname:$CADDY_HTTPPORT {
    gzip {
        level 5
        min_length 1400
    }
    #browse
    header / {
        Cache-Control "max-age=86400"
        #X-Content-Type-Options "nosniff"
        #X-Frame-Options "SAMEORIGIN"
        #X-XSS-Protection "1; mode=block"
        X-Powered-By "Caddy via CentminMod"
        #-Server
    }
    timeouts {
    read   10s
    header 10s
    write  20s
    idle   2m
    }
    tls off
    root /home/nginx/domains/${vhostname}/public
    fastcgi / 127.0.0.1:9000 {
        ext   .php
        split .php
        index index.php
        connect_timeout 10s
        read_timeout 10s
        send_timeout 10s
    }
    # for wordpress
    #rewrite {
    #    if {path} not_match ^\/wp-admin
    #    to {path} {path}/ /index.php?_url={uri}
    #}
    errors /home/nginx/domains/${vhostname}/log/caddy-${vhostname}-errors.log {
        rotate_size 100 # Rotate after 100 MB
        rotate_age  14  # Keep log files for 14 days
        rotate_keep 10  # Keep at most 10 log files
        rotate_compress
        #404 404.html # Not Found
        #500 500.html # Internal Server Error
      }
    log / /home/nginx/domains/${vhostname}/log/caddy-${vhostname}-access.log "{remote} {when} {method} {uri} {proto} {status} {size} {>User-Agent} {latency}" {
        rotate_size 100 # Rotate after 100 MB
        rotate_age  14  # Keep log files for 14 days
        rotate_keep 10  # Keep at most 10 log files
        rotate_compress
    }
}
EOF
  echo "/etc/caddy/conf.d/${vhostname}.conf created on $CADDY_HTTPPORT"
else
  if [[ -f "/etc/caddy/conf.d/${vhostname}.conf" ]]; then
    echo "/etc/caddy/conf.d/${vhostname}.conf already exists"
  fi
  if [ ! -d "/home/nginx/domains/${vhostname}/public" ]; then
    echo "directory /home/nginx/domains/${vhostname}/public not found"
  fi
fi

if [[ "$SELFSIGN_CERT" = [yY] ]]; then
  SELFSIGNOPT=' self_signed'
else
  SELFSIGNOPT=""
fi

if [ ! -f "/etc/caddy/conf.d/${vhostname}.ssl.conf" ] && [ -d "/home/nginx/domains/${vhostname}/public" ]; then
cat >"/etc/caddy/conf.d/${vhostname}.ssl.conf"<<EOF
$vhostname:$CADDY_HTTPSPORT {
    gzip {
        level 5
        min_length 1400
    }
    #browse
    header / {
        #Strict-Transport-Security "max-age=31536000"
        Cache-Control "max-age=86400"
        #X-Content-Type-Options "nosniff"
        #X-Frame-Options "SAMEORIGIN"
        #X-XSS-Protection "1; mode=block"
        X-Powered-By "Caddy via CentminMod"
        #-Server
    }
    timeouts {
    read   10s
    header 10s
    write  20s
    idle   2m
    }   
    tls${SELFSIGNOPT} {
        protocols tls1.0 tls1.2
        ciphers $CIPHERLIST
    }
    root /home/nginx/domains/${vhostname}/public
    fastcgi / 127.0.0.1:9000 {
        ext   .php
        split .php
        index index.php
        connect_timeout 10s
        read_timeout 10s
        send_timeout 10s
    }
    # for wordpress
    #rewrite {
    #    if {path} not_match ^\/wp-admin
    #    to {path} {path}/ /index.php?_url={uri}
    #}
    errors /home/nginx/domains/${vhostname}/log/caddy-${vhostname}-errors.log {
        rotate_size 100 # Rotate after 100 MB
        rotate_age  14  # Keep log files for 14 days
        rotate_keep 10  # Keep at most 10 log files
        rotate_compress
        #404 404.html # Not Found
        #500 500.html # Internal Server Error
      }
    log / /home/nginx/domains/${vhostname}/log/caddy-${vhostname}-access.ssl.log "{remote} {when} {method} {uri} {proto} {status} {size} {>User-Agent} {latency}" {
        rotate_size 100 # Rotate after 100 MB
        rotate_age  14  # Keep log files for 14 days
        rotate_keep 10  # Keep at most 10 log files
        rotate_compress
    }
}
EOF
  echo "/etc/caddy/conf.d/${vhostname}.ssl.conf created on $CADDY_HTTPSPORT"
  SSLVHOST_EXISTS='y'
else
  if [[ -f "/etc/caddy/conf.d/${vhostname}.ssl.conf" ]]; then
    echo "/etc/caddy/conf.d/${vhostname}.ssl.conf already exists"
  fi
  if [ ! -d "/home/nginx/domains/${vhostname}/public" ]; then
    echo "directory /home/nginx/domains/${vhostname}/public not found"
  fi
fi

if [[ ! -d "$CADDY_CERTHOME/${vhostname}" ]]; then
  mkdir -p "$CADDY_CERTHOME/${vhostname}"
fi

if [[ ! -f "$CADDY_CERTHOME/${vhostname}" ]]; then
  touch "$CADDY_CERTHOME/${vhostname}/${vhostname}.conf"
fi

if [[ "$SSLVHOST_EXISTS" = [yY] ]]; then
  echo
  echo | openssl s_client -connect "${vhostname}":"${CADDY_HTTPSPORT}" 2>/dev/null | openssl x509 -noout -dates > "$CADDY_CERTHOME/${vhostname}/${vhostname}.conf"
  acmecreate_date=$(grep "^notBefore=" "$CADDY_CERTHOME/${vhostname}/${vhostname}.conf" | cut -d '=' -f 2)
  acmenextrenew_date=$(grep "^notAfter=" "$CADDY_CERTHOME/${vhostname}/${vhostname}.conf" | cut -d '=' -f 2)
  echo "
$vhostname SSL Cert Created: $acmecreate_date
$vhostname SSL Cert Next Renewal Date: $acmenextrenew_date
"
  if [[ "$PUSHALERT" = [yY] ]]; then
    pushover_alert $vhostname $CADDY_HTTPSPORT
  fi
fi

if [[ -f "/etc/caddy/conf.d/${vhostname}.conf" ]] && [[ -f "/etc/caddy/conf.d/${vhostname}.ssl.conf" ]]; then
  echo
  c_restart
fi
}

######################################################
case "$1" in
  install )
  getcaddy
  setupcaddy
  mainvhost_setup
    ;;
  update )
  c_update
    ;;
  uninstall )
  c_uninstall
    ;;
  addsite )
  mkvhost
    ;;
  start )
  c_start
    ;;
  stop )
  c_stop
    ;;
  restart )
  c_restart
    ;;
  status )
  c_status
    ;;
  domains )
  c_domains
    ;;
  certs )
  c_certs
    ;;
  version )
  c_ver
    ;;
  -help|-h )
  *
    helpmsg
    ;;
  * )
    helpmsg
    ;;
esac